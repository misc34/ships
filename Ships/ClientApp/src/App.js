import React, { Component } from 'react';
import { Container } from "@mui/material";
import Grid from "@mui/material/Grid";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import TypoGraphy from '@material-ui/core/Typography';
import ShipComponent from './components/ShipComponent';

import './custom.css';

export default class App extends Component { 
  static displayName = App.name;

  render () {
      return (
          <div>
            <header>
                <AppBar>
                    <Toolbar>
                        <TypoGraphy variant="h5" color="inherit">
                                Ship Creator
                        </TypoGraphy>
                    </Toolbar>
                </AppBar>
            </header>
            <Container maxWidth='sm'>
                <Grid
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                    style={{ minHeight: '100vh' }}
                >
                    <Grid item xs={3}>
                        <ShipComponent />
                    </Grid>
                </Grid>
        </Container>
      </div>
    );
  }
}
