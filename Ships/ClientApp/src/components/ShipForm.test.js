﻿import React from 'react'
import '@testing-library/jest-dom'
import { render, waitFor, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ShipForm from './ShipForm';
import { validationErrorMsgs as errorMsgs } from './validators/ShipValidator'

describe('ShipComponent', () => {

    it('is rendered and shows no errors', async () => {

        renderDefaultComponent();

        shipFormRenderedSuccessful()
    });


    it('code field shows error message on wrong input and disables buttons', async () => {

        renderDefaultComponent();

        const codeTextBefore = screen.queryByTestId('codeText');
        expect(within(codeTextBefore).queryByText(errorMsgs.codeMsg)).not.toBeInTheDocument();

        await waitFor(() => {
            userEvent.type(screen.queryByLabelText(/Code/), '{backspace}')
        });

        const codeTextAfter = screen.queryByTestId('codeText');

        expect(within(codeTextAfter).queryByText(errorMsgs.codeMsg)).toBeInTheDocument();
        expect(screen.queryByTestId('createButton')).toBeDisabled();
        expect(screen.queryByTestId('updateButton')).toBeDisabled();
    });

    it('name field shows error message on wrong input and disables buttons', async () => {

        renderDefaultComponent();

        const nameTextBefore = screen.queryByTestId('nameText');
        expect(within(nameTextBefore).queryByText(errorMsgs.nameMsg)).not.toBeInTheDocument();

        await waitFor(() => {
            userEvent.type(screen.queryByLabelText(/Name/), '1')
        });

        const nameTextAfter = screen.queryByTestId('nameText');

        expect(within(nameTextAfter).queryByText(errorMsgs.nameMsg)).toBeInTheDocument();
        expect(screen.queryByTestId('createButton')).toBeDisabled();
        expect(screen.queryByTestId('updateButton')).toBeDisabled();
    });

    it('length field shows error message on wrong input and disables buttons', async () => {

        renderDefaultComponent();

        const lengthTextBefore = screen.queryByTestId('lengthText');
        expect(within(lengthTextBefore).queryByText(errorMsgs.lengthMsg)).not.toBeInTheDocument();

        await waitFor(() => {
            userEvent.type(screen.queryByRole('spinbutton', { name: /length \(in meters\)/i }), '{home}-')
        });

        const lengthTextAfter = screen.queryByTestId('lengthText');

        expect(within(lengthTextAfter).queryByText(errorMsgs.lengthMsg)).toBeInTheDocument();
        expect(screen.queryByTestId('createButton')).toBeDisabled();
        expect(screen.queryByTestId('updateButton')).toBeDisabled();
    });

    it('width field shows error message on wrong input and disables buttons', async () => {

        renderDefaultComponent();

        const widthTextBefore = screen.queryByTestId('widthText');
        expect(within(widthTextBefore).queryByText(errorMsgs.widthMsg)).not.toBeInTheDocument();

        await waitFor(() => {
            userEvent.type(screen.queryByRole('spinbutton', { name: /width \(in meters\)/i }), '{home}-')
        });

        const widthTextAfter = screen.queryByTestId('widthText');

        expect(within(widthTextAfter).queryByText(errorMsgs.widthMsg)).toBeInTheDocument();
        expect(screen.queryByTestId('createButton')).toBeDisabled();
        expect(screen.queryByTestId('updateButton')).toBeDisabled();
    });

    describe('button onClick actions are correct', () => {

        beforeEach(() => {
            fetch.resetMocks();
        });

        it('Create button triggers POST to / ships', async () => {

            fetch.once([JSON.stringify(
                defaultFormValues)
            ],{ status: 200 });

            let refreshToggle = false;
            const refresh = () => refreshToggle = true;

            render(<ShipForm refreshList={refresh} defaultFormValues={defaultFormValues} />);

            await waitFor(() => {
                userEvent.click(screen.queryByTestId('createButton'))
            });

            expect(refreshToggle).toBe(true)
            expect(fetch).toBeCalled()
            expect(fetch).toBeCalledWith('ship', {
                "body": JSON.stringify(defaultFormValues),
                "headers": { "Content-Type": "application/json" },
                "method": "POST"
            })
            shipFormRenderedSuccessful();
        });

        it('Update button triggers PUT to /ships/{code}', async () => {

            fetch.once([,
                { status: 200 }
            ]);

            let refreshCounter= 0;
            const refresh = () => refreshCounter = refreshCounter+1;

            render(<ShipForm refreshList={refresh} defaultFormValues={defaultFormValues} />);

            await waitFor(() => {
                userEvent.click(screen.queryByTestId('updateButton'))
            });

            expect(refreshCounter).toBe(1)
            expect(fetch).toBeCalled()
            expect(fetch).toBeCalledWith('ship/AAAA-1111-A1', {
                "body": JSON.stringify(defaultFormValues),
                "headers": { "Content-Type": "application/json" },
                "method": "PUT"
            })
            shipFormRenderedSuccessful();
        });
    });
});


const renderDefaultComponent = () => render(<ShipForm refreshList={() => { }} defaultFormValues={defaultFormValues} />);

const defaultFormValues = {
    name: "Ship",
    lengthInMeters: 10,
    widthInMeters: 1,
    code: "AAAA-1111-A1"
};

export const shipFormRenderedSuccessful = () => {
    const codeText = screen.queryByTestId('codeText');
    expect(within(codeText).queryByText(errorMsgs.codeMsg)).not.toBeInTheDocument();
    const nameText = screen.queryByTestId('nameText');
    expect(within(nameText).queryByText(errorMsgs.nameMsg)).not.toBeInTheDocument();
    const lengthText = screen.queryByTestId('lengthText');
    expect(within(lengthText).queryByText(errorMsgs.lengthMsg)).not.toBeInTheDocument();
    const widthText = screen.queryByTestId('widthText');
    expect(within(widthText).queryByText(errorMsgs.widthMsg)).not.toBeInTheDocument();
    expect(screen.queryByTestId('createButton')).toBeEnabled();
    expect(screen.queryByTestId('updateButton')).toBeEnabled();
};