﻿import { Stack } from "@mui/material";
import React, { useState } from "react";
import ShipForm from './ShipForm';
import ShipList from './ShipList';

const defaultFormValues = {
    name: "Titanic",
    lengthInMeters: 269,
    widthInMeters: 28,
    code: "OHNO-1912-N8"
};

const ShipComponent = () => {

    const [refreshToggle, setRefreshToggle] = useState(false);

    const refresh = () => {
        return setRefreshToggle(!refreshToggle);

    };

    return (
        <Stack
            spacing={4}
            style={{ minWidth: "500px" }}>
            <ShipForm
                refreshList={refresh}
                defaultFormValues={defaultFormValues} />
            <ShipList
                refreshList={refresh}
                refreshToggle={refreshToggle} />
            </Stack>
    );
};

export default ShipComponent;