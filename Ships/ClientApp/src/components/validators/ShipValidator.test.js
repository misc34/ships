﻿
import { shipValidator } from './ShipValidator';


describe('ShipValidator', () => {

    describe('code validation', () => {

        it.each`
          code                   |  valid
          ${"AAAA-9999-A9"}      |  ${true}
          ${"zzzz-0000-z0"}      |  ${true}
          ${"AAAA-0000-A"}       |  ${false}
          ${"AAAA-0000-0"}       |  ${false}
          ${"AAAA-000-A0"}       |  ${false}
          ${"AAA-0000-A0"}       |  ${false}
          ${"0AAA-0000-A0"}      |  ${false}
          ${"A0AA-0000-A0"}      |  ${false}
          ${"AA0A-0000-A0"}      |  ${false}
          ${"AAA0-0000-A0"}      |  ${false}
          ${"AAAA-A000-A0"}      |  ${false}
          ${"AAAA-0A00-A0"}      |  ${false}
          ${"AAAA-00A0-A0"}      |  ${false}
          ${"AAAA-000A-A0"}      |  ${false}
          ${"AAAA-0000-00"}      |  ${false}
          ${"AAAA-0000-AA"}      |  ${false}
          ${"AAAA-0000-A0A"}     |  ${false}
          ${"AAAA-0000-A00"}     |  ${false}
        `('validating the code: $code should return valid: $valid ', ({ code, valid }) => {

            const isValid = shipValidator.isValidSync({ code: code }) ? true : false;

            expect(isValid).toBe(valid)
        });
    });

    describe('name validation', () => {

        it.each`
          name                   |  valid
          ${"name"}              |  ${true}        
          ${"NAME"}              |  ${true}        
          ${"ANOTHER NAME"}      |  ${true}        
          ${""}                  |  ${false}        
          ${"name no 5"}         |  ${false}        
          ${"äöü"}               |  ${false}        
          ${"%&/("}              |  ${false}        
        `('validating the name: $name should return valid: $valid ', ({ name, valid }) => {

            const isValid = shipValidator.isValidSync({ name: name }) ? true : false;

            expect(isValid).toBe(valid)
        });
    });

    describe('length validation', () => {

        it.each`
          length                   |  valid
          ${1}                     |  ${true}     
          ${99999999}              |  ${true}     
          ${""}                    |  ${false}
          ${"number"}              |  ${false}
          ${0}                     |  ${false}
          ${-1}                    |  ${false}
          ${1.5}                   |  ${false}
        `('validating the length: $length should return valid: $valid ', ({ length, valid }) => {

            const isValid = shipValidator.isValidSync({ lengthInMeters: length }) ? true : false;

            expect(isValid).toBe(valid)
        });
    });

    describe('width validation', () => {

        it.each`
          width                    |  valid
          ${1}                     |  ${true}     
          ${99999999}              |  ${true}     
          ${""}                    |  ${false}
          ${"number"}              |  ${false}
          ${0}                     |  ${false}
          ${-1}                    |  ${false}
          ${1.5}                   |  ${false}
        `('validating the width: width should return valid: $valid ', ({ width, valid }) => {

            const isValid = shipValidator.isValidSync({ widthInMeters: width }) ? true : false;

            expect(isValid).toBe(valid)
        });
    });
});

    