﻿import * as yup from 'yup';

export const validationErrorMsgs = {
    nameMsg: "Latin chars and SPACE only",
    codeMsg: "Code format: AAAA-1111-A1",
    lengthMsg: "Only positive whole numbers",
    widthMsg: "Only positive whole numbers"
}

export const shipValidator =
    yup.object().shape({
        name: yup.string().matches(/^[A-Za-z]+([\ A-Za-z]+)*$/, validationErrorMsgs.nameMsg),
        code: yup.string().matches(/^[A-Za-z]{4}-\d{4}-[A-Za-z]\d$/, validationErrorMsgs.codeMsg),
        lengthInMeters: yup.number().integer().moreThan(0, validationErrorMsgs.lengthMsg),
        widthInMeters: yup.number().integer().moreThan(0, validationErrorMsgs.widthMsg)
    })

