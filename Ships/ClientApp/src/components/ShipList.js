﻿import React, { useState, useEffect } from "react";
import List from '@mui/material/List';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import DirectionsBoatFilledRoundedIcon from '@mui/icons-material/DirectionsBoatFilledRounded';

const defaultValues = {
    ships: []
};

const defaultSelection = {
    index: -1,
    code: ''
}

const ShipList = (props) => {

    const { refreshList, refreshToggle } = props;

    const [listValues, setListValues] = useState(defaultValues);
    const [selectedItem, setSelectedItem] = useState(defaultSelection);

    useEffect(() => {
        loadShips();
    }, [refreshToggle]);

    const handleDelete = async (event) => {
        event.preventDefault();
        if (selectedItem.code != '') {
            const requestOptions = {
                method: 'DELETE'
            };
            await fetch('ship/' + selectedItem.code, requestOptions);
            setSelectedItem({ index: -1, code: '' });
            refreshList();
        }
    };

    const selectItem = (event, index, code) => {
        setSelectedItem({ index: index, code: code });
    };

    const loadShips = async () => {
        const response = await fetch('ships');
        const ships = await response.json();
        setListValues({
            ships: ships,
        });
    };

    const toString = (ship) => {
        return Object.entries(ship).map(([key, value]) => {
            return key + ": " + value + " ";
        })
    }

    return (
        <div>
            <List>
                {listValues.ships.map((ship, index) =>
                    <ListItem disablePadding key={ship.code}>
                        <ListItemButton
                            selected={selectedItem.index === index}
                            onClick={(event) => selectItem(event, index, ship.code)}
                        >
                            <ListItemIcon>
                                <DirectionsBoatFilledRoundedIcon />
                            </ListItemIcon>
                            <ListItemText
                                data-testid={'listItemText' + index}
                                primary={toString(ship)} />
                        </ListItemButton>
                    </ListItem>
                )}
            </List>
            <Stack
                alignItems="center">
                <Button
                    data-testid='deleteButton'
                    variant="contained"
                    color="primary"
                    onClick={handleDelete}>
                    Delete
                </Button>
            </Stack>
        </div>
    );
};

export default ShipList;