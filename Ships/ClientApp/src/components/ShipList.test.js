﻿import React, { useState } from "react";
import '@testing-library/jest-dom'
import { render, waitFor, screen, within, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ShipList from './ShipList';

describe('ShipList', () => {

    beforeEach(() => {
        fetch.resetMocks();
    });

    it('is rendered with data from initial call to GET /ships', async () => {

        fetch.once([JSON.stringify(
            listWithOneShip)
        ]);

        await waitFor(() => {
            render(<ShipList refreshList={() => { }} refreshToggle={true}/>)
        });

        const shipButton0 = screen.queryByTestId('listItemText0');
        expect(within(shipButton0).queryByText(/name: ship lengthinmeters: 10 widthinmeters: 1 code: aaaa\-1111\-a1/i)).toBeInTheDocument();
        expect(screen.queryByTestId('deleteButton')).toBeEnabled();   
    });

    it('calls refreshList when element is deleted', async () => {

        fetch.once([JSON.stringify(listWithOneShip)])
            .once([JSON.stringify([])]);

        let refreshCounter = 0;
        const refresh = () => refreshCounter = refreshCounter+1;

        await waitFor(() => {
            render(<ShipList refreshList={refresh} refreshToggle={true}/>)
        });

        await waitFor(() => {
            userEvent.click(screen.queryByTestId('listItemText0'))
        });
        await waitFor(() => {
            userEvent.click(screen.queryByTestId('deleteButton'))
        });

        expect(refreshCounter).toBe(1);
    });

    it('refreshes on parent state change', async () => {

        fetch.once([JSON.stringify(listWithTwoShips)])
            .once([JSON.stringify(listWithOneShip)])
            .once([JSON.stringify(listWithOneShip)]);

        const Parent = () => {
            const [refreshToggle, setRefreshToggle] = useState(false);

            const refresh = () => setRefreshToggle(!refreshToggle);

            return (
                <div>
                    <ShipList refreshList={refresh} refreshToggle={refreshToggle} />
                </div>
            );
        };
                
        await waitFor(() => {
            render(<Parent />)
        });

        expect(screen.queryByTestId('listItemText1')).toBeInTheDocument();
        
        await waitFor(() => {
            userEvent.click(screen.queryByTestId('listItemText0'))
        });
        await waitFor(() => {
            userEvent.click(screen.queryByTestId('deleteButton'))
        });

        expect(screen.queryByTestId('listItemText1')).not.toBeInTheDocument();
    });
});

const listWithOneShip = [{
    name: "Ship",
    lengthInMeters: 10,
    widthInMeters: 1,
    code: "AAAA-1111-A1"
}];

const listWithTwoShips = [
    {
        name: "Ship",
        lengthInMeters: 10,
        widthInMeters: 1,
        code: "AAAA-1111-A1"
    },
    {
        name: "Another Ship",
        lengthInMeters: 100,
        widthInMeters: 10,
        code: "AAAA-1111-A2"
    }];