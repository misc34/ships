﻿import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Stack from '@mui/material/Stack';
import { shipValidator } from './validators/ShipValidator';



const defaultValidationState = {
    name: { error: false, text: "" },
    lengthInMeters: { error: false, text: "" },
    widthInMeters: { error: false, text: "" },
    code: { error: false, text: "" }
};

const ShipForm = (props) => {

    const { refreshList, defaultFormValues } = props;

    const [formValues, setFormValues] = useState(defaultFormValues);
    const [formValidation, setFormValidation] = useState(defaultValidationState);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormValues({
            ...formValues,
            [name]: value,
        });
        validateField(name, value);
    };

    const validateField = (name, value) => {
        shipValidator.validate({ [name]: value })
            .then((valid) => setFormValidation({
                ...formValidation,
                    [name]: {
                        error: !valid,
                        text: ""}
            }))
            .catch(function (err) {
                setFormValidation({
                    ...formValidation,
                    [name]: {
                        error: true,
                        text: err.errors
                    }
                })
            });
    }

    const validateShip = () => shipValidator.isValidSync(formValues) ? false : true;
    

    const handleSave = async (event) => {
        event.preventDefault();
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formValues)
        };
        await fetch('ship', requestOptions)
        refreshList();
    };

    const handleUpdate = async (event) => {
        event.preventDefault();
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formValues)
        };
        await fetch('ship/' + formValues.code, requestOptions)
        refreshList();
    };
    
    return (
        <div>
            <Stack
                spacing={2}>
                <TextField
                        data-testid = 'codeText'
                        id="code-input"
                        name="code"
                        label="Code"
                        type="text"
                        value={formValues.code}
                        onChange={handleInputChange}
                        helperText={formValidation.code.text}
                        error={formValidation.code.error }
                    />
                <TextField
                        data-testid='nameText'
                        id="name-input"
                        name="name"
                        label="Name"
                        type="text"
                        value={formValues.name}
                        onChange={handleInputChange}
                        helperText={formValidation.name.text}
                        error={formValidation.name.error}
                    />
                <TextField
                        data-testid='lengthText'
                        id="length-input"
                        name="lengthInMeters"
                        label="Length (in meters)"
                        type="number"
                        value={formValues.lengthInMeters}
                        onChange={handleInputChange}
                        helperText={formValidation.lengthInMeters.text}
                        error={formValidation.lengthInMeters.error}
                    />
                <TextField
                        data-testid='widthText'
                        id="width-input"
                        name="widthInMeters"
                        label="Width (in meters)"
                        type="number"
                        value={formValues.widthInMeters}
                        onChange={handleInputChange}
                        helperText={formValidation.widthInMeters.text}
                        error={formValidation.widthInMeters.error}
                    />
                <Stack
                    spacing={2}>
                    <Button
                        data-testid='createButton'
                        variant="contained"
                        color="primary"
                        onClick={handleSave}
                        disabled={validateShip()}>
                        Create Ship
                    </Button>
                    <Button
                        data-testid='updateButton'
                        variant="contained"
                        color="primary"
                        onClick={handleUpdate}
                        disabled={validateShip()}>
                        Update Ship
                    </Button>
                </Stack>
            </Stack>
        </div >
    );
}

export default ShipForm;