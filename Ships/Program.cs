using FluentValidation;
using Microsoft.EntityFrameworkCore;


var builder = WebApplication.CreateBuilder(args);
// swagger
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// ---

builder.Services.AddValidatorsFromAssembly(typeof(Program).Assembly);
// db
builder.Services.AddDbContext<ShipDbContext>(opt =>
    opt.UseInMemoryDatabase("ShipDb"));

builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");

// swagger
app.UseSwagger();

app.MapShipsEndpoints();

// swagger
app.UseSwaggerUI();

app.Run();

// allow access from tests
public partial class Program { }