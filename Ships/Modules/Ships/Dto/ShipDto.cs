﻿namespace Ships.Modules.Ships.Dto
{
    public record ShipDto(string Name, int LengthInMeters, int WidthInMeters, string Code);
}
