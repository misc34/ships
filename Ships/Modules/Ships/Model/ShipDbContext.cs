using Microsoft.EntityFrameworkCore;


    public class ShipDbContext : DbContext
    {
        public ShipDbContext(DbContextOptions<ShipDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder){
            modelBuilder.Entity<ShipEntity>()
                .HasKey(ship => ship.Code);
        }

        public DbSet<ShipEntity> Ships { get; set; } = null!;
    }
