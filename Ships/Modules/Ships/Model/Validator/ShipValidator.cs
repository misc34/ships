﻿using FluentValidation;
using Ships.Modules.Ships.Dto;

namespace Ships.Modules.Ships.Model
{
    public class ShipValidator : AbstractValidator<ShipDto>
    {
        public ShipValidator()
        {
            RuleFor(ship => ship.Name).Matches("^[A-Za-z]+([\\ A-Za-z]+)*$");
            RuleFor(ship => ship.Code).Matches("^[A-Za-z]{4}-\\d{4}-[A-Za-z]\\d$");
            RuleFor(ship => ship.LengthInMeters).GreaterThan(0);
            RuleFor(ship => ship.WidthInMeters).GreaterThan(0);
        }

    }
}
