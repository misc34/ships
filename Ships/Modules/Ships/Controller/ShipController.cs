
using MiminalApis.Validators.FluentValidation;
using MinimalApis.Extensions.Results;
using Ships.Modules.Ships.Dto;

public static class ShipsModule{

    public static IEndpointRouteBuilder MapShipsEndpoints(this IEndpointRouteBuilder endpoints)
    {
        // TODO: use Automapper

        endpoints.MapGet("/ships", (ShipDbContext context) => 
            context.Ships.ToList().Select(ship => new ShipDto(ship.Name, ship.LengthInMeters, ship.WidthInMeters, ship.Code))
        );

        endpoints.MapPut("/ship/{code}", Results<BadRequest, NotFound, Ok> (string code, ShipDto ship, ShipDbContext context) => {
            if(code != ship.Code){
                return Results.Extensions.BadRequest("Code in path does not match code of ship");
            }

            if(context.Ships.Find(ship.Code) is ShipEntity existingShip){
                context.Ships.Remove(existingShip);
                context.Ships.Add(new ShipEntity(ship.Name, ship.LengthInMeters, ship.WidthInMeters, ship.Code));
                context.SaveChanges();
                return Results.Extensions.Ok();
            }
            else return Results.Extensions.NotFound();
        }).WithValidator<ShipDto>();

        endpoints.MapPost("/ship", Results<Conflict, Ok<ShipDto>>(ShipDto ship, ShipDbContext context) => {
            if (context.Ships.Find(ship.Code) == null){
                context.Ships.Add(new ShipEntity(ship.Name, ship.LengthInMeters, ship.WidthInMeters, ship.Code));
                context.SaveChanges();
                return Results.Extensions.Ok(ship);
            }
            else return Results.Extensions.Conflict("There already exists a ship with the given code");
        }).WithValidator<ShipDto>();

        endpoints.MapDelete("/ship/{code}", Results<NotFound, NoContent> (string code, ShipDbContext context) => {
             if(context.Ships.Find(code) is ShipEntity existingShip ){
                context.Ships.Remove(existingShip);
                context.SaveChanges();
                return Results.Extensions.NoContent();
             }
             else return Results.Extensions.NotFound();
        });

        return endpoints;
    }

}


/*
Your Task

Create a simple application that illustrates your skills. Please use react and .net core.

The application will allow the user to perform CRUD (Create, Read, Update & Delete) operations on a ship.  Each ship must have a name (string), length (in metres), 
width (in metres) and code (a string with a format of AAAA-1111-A1 where A is any character from the Latin alphabet and 1 is a number from 0 to 9).

The deliverable must be a public GitHub repository. You can assume that reviewers will have Visual Studio Code and Docker installed. You must create a readme that contains all instructions required to run your application. 
No infrastructural requirements are permitted – your solution must run locally. We would suggest that you mock the database (memory or file is fine).

Please show us what you can achieve - your best effort, not just getting the task done. You may take as long as you feel is appropriate.
*/