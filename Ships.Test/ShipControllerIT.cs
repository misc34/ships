using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Ships.Modules.Ships.Dto;
using Xunit;

public class ShipControllerIT
{

    [Fact]
    public async Task GetShips_empty_when_no_ships_created()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        // when
        var ships = await client.GetFromJsonAsync<List<ShipDto>>("/ships");
        // then
        Assert.Empty(ships);
    }

    [Fact]
    public async Task GetShips_returns_list_of_existing_ships()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var shipToCreate = new ShipDto("shipname", 20, 10, "AAAA-1111-A1");
        var response = await client.PostAsJsonAsync("/ship", shipToCreate);

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);

        // when
        var ships = await client.GetFromJsonAsync<List<ShipDto>>("/ships");

        // then
        var shipResult = Assert.Single(ships);
        assertShipsAreEqual(shipToCreate, shipResult);
    }

    [Fact]
    public async Task PostShip_creates_new_ship_if_it_does_not_exist()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        // when
        var shipToCreate = new ShipDto("ship name", 20, 10, "AAAA-1111-A1");
        var response = await client.PostAsJsonAsync("/ship", shipToCreate);

        // then
        var newShip = await response.Content.ReadAsAsync<ShipDto>();

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        assertShipsAreEqual(shipToCreate, newShip);

        var ships = await client.GetFromJsonAsync<List<ShipDto>>("/ships");

        var shipResult = Assert.Single(ships);
        assertShipsAreEqual(shipToCreate, shipResult);
    }

    [Fact]
    public async Task PostShip_responds_with_409_if_ship_already_exists()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var shipToCreate = new ShipDto("ship name", 20, 10, "AAAA-1111-A1");
        await createShip(client, shipToCreate);

        // when
        var response = await client.PostAsJsonAsync("/ship", shipToCreate);

        // then
        Assert.Equal(HttpStatusCode.Conflict, response.StatusCode);
    }

    [Fact]
    public async Task PostShip_responds_with_400_if_ship_is_not_valid()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var shipToCreate = new ShipDto("ship name", 20, 10, "this is not a valid code");

        // when
        var response = await client.PostAsJsonAsync("/ship", shipToCreate);

        // then
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    [Fact]
    public async Task PutShip_updates_existing_ship()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var code = "AAAA-1111-A1";
        var shipToCreate = new ShipDto("ship name", 20, 10, code);
        await createShip(client, shipToCreate);
               
        // when
        var updatedShip = new ShipDto("other ship name", 99, 22, code);
        var response = await client.PutAsJsonAsync("/ship/"+code, updatedShip);

        // then
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
    }

    [Fact]
    public async Task PutShip_returns_400_when_code_in_path_does_not_match_code_in_ship()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var code = "AAAA-1111-A1";
        var shipToCreate = new ShipDto("ship name", 20, 10, code);
        await createShip(client, shipToCreate);

        // when
        var codeInPath = "AAAA-1111-A2";
        var updatedShip = new ShipDto("other ship name", 99, 22, code);
        var response = await client.PutAsJsonAsync("/ship/" + codeInPath, updatedShip);

        // then
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    [Fact]
    public async Task PutShip_returns_400_when_ship_is_not_valid()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var code = "AAAA-1111-A1";
        var shipToCreate = new ShipDto("ship name", 20, 10, code);
        await createShip(client, shipToCreate);

        // when
        var updatedShip = new ShipDto("8 ball", 99, 22, code);
        var response = await client.PutAsJsonAsync("/ship/" + code, updatedShip);

        // then
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    [Fact]
    public async Task PutShip_returns_404_when_ship_does_not_exist()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var code = "AAAA-1111-A1";

        // when
        var updatedShip = new ShipDto("other ship name", 99, 22, code);
        var response = await client.PutAsJsonAsync("/ship/" + code, updatedShip);

        // then
        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }

    [Fact]
    public async Task DeleteShip_deletes_ship()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var code = "AAAA-1111-A1";
        var shipToCreate = new ShipDto("ship name", 20, 10, code);
        await createShip(client, shipToCreate);

        // when
        var response = await client.DeleteAsync("/ship/" + code);

        // then
        Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

        var ships = await client.GetFromJsonAsync<List<ShipDto>>("/ships");

        Assert.Empty(ships);
    }

    [Fact]
    public async Task DeleteShip_returns_404_when_ship_with_code_does_not_exist()
    {
        // given
        await using var application = new ShipsApplication();
        var client = application.CreateClient();

        var ships = await client.GetFromJsonAsync<List<ShipDto>>("/ships");

        Assert.Empty(ships);

        // when
        var codeInPath = "AAAA-1111-A1";
        var response = await client.DeleteAsync("/ship/" + codeInPath);

        // then
        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }

    private async Task createShip(HttpClient client, ShipDto ship)
    {
        var response = await client.PostAsJsonAsync("/ship", ship);

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
    }

    private void assertShipsAreEqual(ShipDto expected, ShipDto actual)
    {
        Assert.Equal(expected.Code, actual.Code);
        Assert.Equal(expected.Name, actual.Name);
        Assert.Equal(expected.LengthInMeters, actual.LengthInMeters);
        Assert.Equal(expected.WidthInMeters, actual.WidthInMeters);
    }

    class ShipsApplication : WebApplicationFactory<Program>
    {
        protected override IHost CreateHost(IHostBuilder builder)
        {
            var root = new InMemoryDatabaseRoot();

            builder.ConfigureServices(services =>
            {
                services.RemoveAll(typeof(DbContextOptions<ShipDbContext>));

                services.AddDbContext<ShipDbContext>(options =>
                    options.UseInMemoryDatabase("Testing", root));
            });

            return base.CreateHost(builder);
        }
    }
}