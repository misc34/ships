﻿using FluentValidation.TestHelper;
using Ships.Modules.Ships.Dto;
using Ships.Modules.Ships.Model;
using Xunit;

namespace Ships.Test
{
    public class ShipValidatorTest
    {
        
        [Theory]
        [InlineData("AAAA-9999-A9")]
        [InlineData("zzzz-0000-z0")]
        public void CodeValidation_valid(string code)
        {
            var validator = new ShipValidator();
            var model = new ShipDto("shipname", 20, 10, code);
            var result = validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(person => person.Code);
        }

        [Theory]
        [InlineData("AAAA-0000-A")]
        [InlineData("AAAA-0000-0")]
        [InlineData("AAAA-000-A0")]
        [InlineData("AAA-0000-A0")]
        [InlineData("0AAA-0000-A0")]
        [InlineData("A0AA-0000-A0")]
        [InlineData("AA0A-0000-A0")]
        [InlineData("AAA0-0000-A0")]
        [InlineData("AAAA-A000-A0")]
        [InlineData("AAAA-0A00-A0")]
        [InlineData("AAAA-00A0-A0")]
        [InlineData("AAAA-000A-A0")]
        [InlineData("AAAA-0000-00")]
        [InlineData("AAAA-0000-AA")]
        [InlineData("AAAA-0000-A0A")]
        [InlineData("AAAA-0000-A00")]
        public void CodeValidation_invalid(string code)
        {
            var validator = new ShipValidator();
            var model = new ShipDto("shipname", 20, 10, code);
            var result = validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(person => person.Code);
        }

        [Theory]
        [InlineData("name")]
        [InlineData("NAME")]          
        [InlineData("ANOTHER NAME")] 
        public void NameValidation_valid(string name)
        {
            var validator = new ShipValidator();
            var model = new ShipDto(name, 20, 10, "AAAA-0000-A0");
            var result = validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(person => person.Name);
        }

        [Theory]
        [InlineData("")]   
        [InlineData("name no 5")]   
        [InlineData("äöü")]   
        [InlineData("%&/(")]     
        public void NameValidation_invalid(string name)
        {
            var validator = new ShipValidator();
            var model = new ShipDto(name, 20, 10, "AAAA-0000-A0");
            var result = validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(person => person.Name);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(99999999)]   
        public void LengthValidation_valid(int length)
        {
            var validator = new ShipValidator();
            var model = new ShipDto("name", length, 10, "AAAA-0000-A0");
            var result = validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(person => person.LengthInMeters);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void LengthValidation_invalid(int length)
        {
            var validator = new ShipValidator();
            var model = new ShipDto("name", length, 10, "AAAA-0000-A0");
            var result = validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(person => person.LengthInMeters);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(99999999)]
        public void WidthValidation_valid(int width)
        {
            var validator = new ShipValidator();
            var model = new ShipDto("name", 10, width, "AAAA-0000-A0");
            var result = validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(person => person.WidthInMeters);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void WidthValidation_invalid(int width)
        {
            var validator = new ShipValidator();
            var model = new ShipDto("name", 10, width, "AAAA-0000-A0");
            var result = validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(person => person.WidthInMeters);
        }
    }
}
