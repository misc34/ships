
# Ship Creator

## Built using

* .NET 6 SDK
* Node.js 16.3.1

## Run App in Docker

1. cd to solution directory
2. build image: 
`docker build -f ./Ships/Dockerfile --force-rm -t ships .`
3. run image
`docker run -p 8080:80 --name Ships ships`
4. open browser and go to localhost:8080

## swagger

Once the app is running a basic swagger api is available at localhost:8080/swagger
